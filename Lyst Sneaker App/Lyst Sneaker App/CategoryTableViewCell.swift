//
//  CategoryTableViewCell.swift
//  Lyst Sneaker App
//
//  Created by Carla on 2017/07/15.
//  Copyright © 2017 Goldsting. All rights reserved.
//

import UIKit
import LystNetworking

class CategoryTableViewCell : UITableViewCell {
    
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var categorySwitch: UISwitch!
    var category : CategoryFilter?
    
    @IBAction func didFlipSwitch(_ sender: AnyObject) {
        guard self.category != nil else {
            return
        }
        
        self.category!.selected = !self.category!.selected
    }
    
    func populate(category: CategoryFilter) {
        self.category = category;
        self.categoryLabel.text = category.label
        if (category.selected) {
            self.categorySwitch.setOn(true, animated: false)
        } else {
            self.categorySwitch.setOn(false, animated: false)
        }
    }
    
}
