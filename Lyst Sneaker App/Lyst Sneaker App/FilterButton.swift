//
//  File.swift
//  Lyst Sneaker App
//
//  Created by Carla on 2017/07/15.
//  Copyright © 2017 Goldsting. All rights reserved.
//

import UIKit

class FilterButton : UIButton {
    
//    let filterButtonBadge : UIView = UIView()
    let filterButtonBadgeLabel : UILabel = UILabel()
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.borderWidth = 0.5
        self.layer.borderColor = UIColor.lightGray.cgColor
        self.layer.cornerRadius = 18.0
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize(width: 0, height: 2)
        self.layer.shadowRadius = 1
//        self.layer.shouldRasterize = true
        
        let buttonWidth = self.frame.size.width
        let buttonHeight = self.frame.size.height
        let frame = CGRect(x: buttonWidth - 25,
                           y: buttonHeight/2 - 10,
                           width: 20,
                           height: 20)
        
        self.filterButtonBadgeLabel.backgroundColor = .blue
        self.filterButtonBadgeLabel.frame = frame;
        self.filterButtonBadgeLabel.layer.cornerRadius = frame.width/2;
        self.filterButtonBadgeLabel.layer.masksToBounds = true;
        self.filterButtonBadgeLabel.textColor = .white
        self.filterButtonBadgeLabel.font = self.filterButtonBadgeLabel.font.withSize(12)
        self.filterButtonBadgeLabel.textAlignment = .center

        self.addSubview(self.filterButtonBadgeLabel);

    }
    
    func setBadgeLabelText(count: Int) {
        if (count > 0) {
            self.filterButtonBadgeLabel.isHidden = false;
            self.filterButtonBadgeLabel.text = String(count)
        } else {
            self.filterButtonBadgeLabel.isHidden = true;
        }
    }
    
}
