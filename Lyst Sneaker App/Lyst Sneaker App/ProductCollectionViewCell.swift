//
//  ProductCollectionViewCell.swift
//  Lyst Sneaker App
//
//  Created by Carla on 2017/07/15.
//  Copyright © 2017 Goldsting. All rights reserved.
//

import UIKit
import LystNetworking

class ProductCollectionViewCell : UICollectionViewCell  {
    
    @IBOutlet weak var imageView: DownloadableImageView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var activityIndiciator: UIActivityIndicatorView!
    
    //NOTE: Could move this into its own ProductCellViewModel
    func populate(product: Product?) {
        
        guard let product = product else {
            return;
        }
        
        self.titleLabel.text = product.title
        self.priceLabel.text = product.price
        
        self.activityIndiciator.startAnimating()
        self.activityIndiciator.isHidden = false
        self.imageView.download(remoteURL: product.imageDownloadURL, localURL: product.imageLocalURL, completion: {
            image in
            
            
            if let image = image {
                DispatchQueue.main.async(execute: { () -> Void in
                    self.activityIndiciator.stopAnimating()
                    self.activityIndiciator.isHidden = true
//                    self.imageView.alpha = 0
                    let resizedImage = self.resizeImageForView(image: image)
                    self.imageView.image = resizedImage
                    
//                    UIView.animate(withDuration: 0.3, animations: {
//                        self.imageView.alpha = 1
//                    })
                    
                })
            }
            
        })
        
    }
    
    
    override func prepareForReuse() {
        self.activityIndiciator.isHidden = true
        super.prepareForReuse()
        self.imageView.image = nil
        self.imageView.cancelDownload()
    }
    
    private func resizeImageForView(image: UIImage) -> UIImage {
        
        let newWidth = self.imageView.frame.size.width;
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        
        image.draw(in: CGRect(x: 0, y: 0,width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        
        
        return newImage!
    }
    
}
