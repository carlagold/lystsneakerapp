//
//  FilterViewModel.swift
//  Lyst Sneaker App
//
//  Created by Carla on 2017/07/15.
//  Copyright © 2017 Goldsting. All rights reserved.
//

import Foundation
import LystNetworking

protocol FilterDelegate {
    var selectedCategories : [CategoryFilter] {get set}
}

class FilterViewModel {
 
    var delegate : FilterDelegate?
    var networkManager : NetworkManager? //to be injected from previous viewModel
    var gender : Gender
    var reloadCallback : (() -> ())?
    var categories : [CategoryFilter] = []
    var selectedCategories : [CategoryFilter] = []
    
    init(networkManager: NetworkManager, gender: Gender) {
        self.networkManager = networkManager;
        self.gender = gender
    }
    
    
    //MARK: Public API
    public func getCategories() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        self.networkManager?.fetchCategories(gender: self.gender.rawValue, callback: { categories, errorMessage in
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            if let categories = categories {
                self.categories = categories
            }
            self.reloadCallback!();
        })
    }
    
    public func getNumberOfItemsInSection(section: Int) -> Int {
        return self.categories.count
    }
    
    public func getCategory(row: Int) -> CategoryFilter {
        let category = self.categories[row]
        if (self.selectedCategories.contains(where: {$0.label == category.label} )) {
            category.selected = true
        }
        return category
    }
    
    public func getSelectedCategories() -> [CategoryFilter] {
        let selected = self.categories.filter { $0.selected }
        return selected;
    }
    
    

}
