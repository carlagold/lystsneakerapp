//
//  FilterViewController.swift
//  Lyst Sneaker App
//
//  Created by Carla on 2017/07/15.
//  Copyright © 2017 Goldsting. All rights reserved.
//

import UIKit
import LystNetworking


class FilterViewController : UIViewController {
    
    @IBOutlet weak var categoryTableView: UITableView!
    var viewModel : FilterViewModel!
    
    @IBAction func didTapDone (_ sender: UIButton) {
        let selectedCategories = self.viewModel?.getSelectedCategories();
        
        self.viewModel.delegate?.selectedCategories = selectedCategories!

        self.dismiss(animated: true)
    }
    
    override func willMove(toParentViewController parent: UIViewController?) {
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configure()
        self.viewModel.reloadCallback = reload
        self.viewModel.getCategories()
    }
    
    func configure() {
        self.categoryTableView.delegate = self
        self.categoryTableView.dataSource = self
    }
    
    func reload() {
        self.categoryTableView.reloadData()
        
    }
}

extension FilterViewController : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.viewModel.getNumberOfItemsInSection(section: section))
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "categoryCell", for: indexPath) as! CategoryTableViewCell
        cell.populate(category: (self.viewModel.getCategory(row: indexPath.row)))
        return cell
    }
    
}
