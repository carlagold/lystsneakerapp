//
//  FeedViewModel.swift
//  Lyst Sneaker App
//
//  Created by Carla on 2017/07/15.
//  Copyright © 2017 Goldsting. All rights reserved.
//

import Foundation
import LystNetworking

enum Gender : String {
    case Female = "F"
    case Male = "M"
}

class FeedViewModel: FilterDelegate {
    
    fileprivate var products = [Product]()
    
    var selectedGender : Gender = .Female

    var networkManager : NetworkManager = NetworkManager() //created here, but will be passed to other viewModels as to ensure we are always using the same instance
    var reloadCallback : (() -> ())!
    var selectedCategories: [CategoryFilter] = []

}

//MARK: Public API
extension FeedViewModel {
    
    public func clearData() {
      //  self.products = []
        self.selectedCategories = []
    }
    
    public func getProducts() {
        self.products = [];
        let categories = self.selectedCategories.map({return $0.value!})
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        self.networkManager.fetchProducts(gender: self.selectedGender.rawValue, categories: categories, callback: { products, error in
            UIApplication.shared.isNetworkActivityIndicatorVisible = false

            if let products = products {
                self.products = products
            }
            self.reloadCallback()

        })
    }
    
    public func numberOfItemsInSection(section : Int) -> Int {
        let count = self.products.count
        return count
    }

    public func itemAtIndex(index: Int) -> Product? {
        if (self.products.count > index) {
            return self.products[index];
        }
        
        return nil;
    }
    
    public func getNextViewModel() -> FilterViewModel {
        let nextViewModel = FilterViewModel(networkManager: self.networkManager, gender: self.selectedGender);
        nextViewModel.selectedCategories = self.selectedCategories;
        return nextViewModel
    }
    
    public func getFilterCount() -> Int {
        return self.selectedCategories.count
    }
    
}
