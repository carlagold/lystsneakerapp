//
//  ViewController.swift
//  Lyst Sneaker App
//
//  Created by Carla on 2017/07/15.
//  Copyright © 2017 Goldsting. All rights reserved.
//

import UIKit

class FeedViewController: UIViewController {
    
    @IBOutlet weak var filterButton: FilterButton!
    @IBOutlet weak var genderSegmentedControl: UISegmentedControl!
    @IBOutlet weak var feedCollection: UICollectionView!
    
    
    var viewModel : FeedViewModel = FeedViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.reloadCallback = finishReloadCollectionView
        self.configure()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.startReloadCollectionView()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func configure() {        
        self.feedCollection.delegate = self
        self.feedCollection.dataSource = self
    }
    
    
    func startReloadCollectionView() {
        self.filterButton.setBadgeLabelText(count: 0)
        self.filterButton.isEnabled = false
        self.genderSegmentedControl.isEnabled = false
        
        UIView.animate(withDuration: 0.3) {
            
            self.filterButton.alpha = 0
            self.feedCollection.alpha = 0
        }
        

        
        viewModel.getProducts()
    }
    
    func finishReloadCollectionView() {
        self.filterButton.setBadgeLabelText(count: self.viewModel.getFilterCount())
        self.filterButton.isEnabled = true
        self.genderSegmentedControl.isEnabled = true
        
        self.feedCollection.reloadData()
        
        if (self.viewModel.numberOfItemsInSection(section: 0) > 0) {

            UIView.animate(withDuration: 0.3) {
                self.filterButton.alpha = 1
                self.feedCollection.alpha = 1
            }
            
            self.feedCollection?.scrollToItem(at: IndexPath(row: 0, section: 0),
                                              at: .top,
                                              animated: true)
        }
    }
    
    
    
    
    
    
    @IBAction func didTapGenderSegmentedControl(_ sender: AnyObject) {
        if (sender.selectedSegmentIndex == 0) {
            viewModel.selectedGender = .Female
        } else if (sender.selectedSegmentIndex == 1)  {
            viewModel.selectedGender = .Male
        }
        self.viewModel.clearData()
        self.startReloadCollectionView()
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowFilters" {
            if let viewController = segue.destination as? FilterViewController {
                viewController.viewModel = self.viewModel.getNextViewModel()
                viewController.viewModel.delegate = self.viewModel
            }
        }
    }
}


extension FeedViewController : UICollectionViewDelegate, UICollectionViewDataSource {
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (self.viewModel.numberOfItemsInSection(section: section))
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "productCell", for: indexPath) as! ProductCollectionViewCell

        cell.populate(product: (self.viewModel.itemAtIndex(index:indexPath.row)))
        
        return cell
    }
}

extension FeedViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        

        let numberOfItemsPerRow = getNumberOfItemsPerRow()
        
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(numberOfItemsPerRow - 1))
        
        let width : CGFloat = (collectionView.bounds.width - totalSpace) / CGFloat(numberOfItemsPerRow)
        let height : CGFloat = 1.2 * width
        
        return CGSize(width: width, height: height)
    }
    
    func getNumberOfItemsPerRow() -> Int {
        
        enum UIUserInterfaceIdiom : Int {
            case unspecified
            case phone // iPhone and iPod touch style UI
            case pad // iPad style UI
        }
        
        
        var numberOfItemsPerRow = 4;
        let orientation =  UIDevice.current.orientation
        
        //iphone, portrait: 2 per row
        //iphone, landscape: 3 per row
        //ipad: 4 per row
        
        switch UIDevice.current.userInterfaceIdiom {
        case .phone:
            // It's an iPhone
            switch orientation {
            case .portraitUpsideDown, .portrait:
                numberOfItemsPerRow = 2
                break
            case .landscapeLeft, .landscapeRight:
                numberOfItemsPerRow = 3
                break
            default:
                numberOfItemsPerRow = 2
                break
            }
            break
        case .pad:
            // It's an iPad
            numberOfItemsPerRow = 4
            break
            
        default: break
            
        }
        
        return numberOfItemsPerRow;
    }
    
    override func willRotate(to toInterfaceOrientation: UIInterfaceOrientation, duration: TimeInterval) {
        self.feedCollection.collectionViewLayout.invalidateLayout()
    }
    
    
}
